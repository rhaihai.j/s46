import { useState } from 'react';

const Home = () => {

    const [course, setCourse] = useState('Phyton')
    const [price, setPrices] = useState('500')

    const handleClick = () =>{
        alert('hello')
        setCourse('Java')
        setPrices(150)
    }
    return(
        <div>
        <h2>Home Page Start Here</h2>
        <span>These are some of our works: {course}</span>
        <br />
        <h4>Price: {price}</h4>
        <button onClick={handleClick}>Click Here</button>
        </div>        
    )
}

export default Home;