import './App.css';
import Nav from './Nav'
import Home from './Home'

function App() {
  // const subTitle = "This is an exmaple react app";
  // const price = 1500;
  // const colors = ['red','green','blue']
  return (
    <div className="App">
     <div className="content">
      <Nav />
      <Home />
     </div>
    </div>
  );
}

export default App;
