const Nav = () => {
    return(
        <nav>
            <h1>Zuitt Devs</h1>
            <div>
                <a href="/">About Us</a>
                <a href="/services">Services</a>
                <hr />
            </div>
        </nav>
    );
}

export default Nav